﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    public class DeckFactory
    {
        private string[] seeds;

        private string[] names;

        public IList<string> Seeds
        {
            get => seeds.ToList();
            set => seeds = value.ToArray();
        }

        public IList<string>Names
        {
           get => names.ToList();
           set => names = value.ToArray();
        }

        public int DeckSize
        {
            get => this.names.Length * this.seeds.Length;
        }

        public ISet<Card> Deck
        {
            get
            {
                if (this.names == null || this.seeds == null)
                {
                    Environment.Exit(0);
                }
                return new HashSet<Card>(Enumerable.Range(0, this.names.Length)
                    .SelectMany(i => Enumerable.Repeat(i, this.seeds.Length)
                        .Zip(Enumerable.Range(0, this.seeds.Length), (n, s) => Tuple.Create(this.names[n], this.seeds[s], n))
                    ).Select(tuple => new Card(tuple))
                    .ToList());
            }
        }
    }

}
